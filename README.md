# Postman Collections

## Big Picture

### Adding Summer Scheule

It adds summer schedule (from Jul 1st to Aug 31th) and standard schuele (from Sep 1st) to a list of Big Picture users.

#### How to use

1. Import `BigPicture.postman_collection.json` in Postman
2. Prepare a csv with a `name` and `id` column for all the users you want to update. `name` is user name, `id` is the BigPicture id. There is a spreadsheet with all the values [here](https://docs.google.com/spreadsheets/d/1DxkMVpj1frMgbOkBOT8Ru-KO1RmTUUhsf3Ko3VOkHkQ/edit#gid=0) Info to generate this list can be obtained executing the `Resorces` request in this collection.
3. Get an auth token: open Big Picture in your browser, in developers tools look for `authorization` Request Header and copy its contents (should be someting like: `BPBearer ...`). Set this value in `auth` variable in the collection.
4. Right click in the Collection, select `Run Collection`
5. Unselect `Resources` request
6. Select the file generated in step 2
7. Click on `Run BigPicture`